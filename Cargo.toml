[package]
name = "conduit"
description = "A Matrix homeserver written in Rust"
license = "Apache-2.0"
authors = ["timokoesters <timo@koesters.xyz>"]
homepage = "https://conduit.rs"
repository = "https://gitlab.com/famedly/conduit"
readme = "README.md"
version = "0.1.0"
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
# Used to handle requests
# TODO: This can become optional as soon as proper configs are supported
rocket = { git = "https://github.com/SergioBenitez/Rocket.git", rev = "93e62c86eddf7cc9a7fc40b044182f83f0d7d92a", features = ["tls"] } # Used to handle requests
#rocket = { git = "https://github.com/timokoesters/Rocket.git", branch = "empty_parameters", default-features = false, features = ["tls"] }

# Used for matrix spec type definitions and helpers
ruma = { git = "https://github.com/ruma/ruma", rev = "c1693569f15920e408aa6a26b7f3cc7fc6693a63", features = ["compat", "rand", "appservice-api-c", "client-api", "federation-api", "push-gateway-api-c", "unstable-pre-spec", "unstable-exhaustive-types"] }
#ruma = { git = "https://github.com/timokoesters/ruma", rev = "220d5b4a76b3b781f7f8297fbe6b14473b04214b", features = ["compat", "rand", "appservice-api-c", "client-api", "federation-api", "push-gateway-api-c", "unstable-pre-spec", "unstable-exhaustive-types"] }
#ruma = { path = "../ruma/ruma", features = ["compat", "rand", "appservice-api-c", "client-api", "federation-api", "push-gateway-api-c", "unstable-pre-spec", "unstable-exhaustive-types"] }

# Used when doing state resolution
state-res = { git = "https://github.com/ruma/state-res", rev = "4516d73e8c7495330619bfb5b42c3bbf704293d8", features = ["unstable-pre-spec"] }
#state-res = { path = "../state-res", features = ["unstable-pre-spec"] }

# Used for long polling and federation sender, should be the same as rocket::tokio
tokio = "1.2.0"
# Used for storing data permanently
sled = { version = "0.34.6", features = ["compression", "no_metrics"] }
#sled = { git = "https://github.com/spacejam/sled.git", rev = "e4640e0773595229f398438886f19bca6f7326a2", features = ["compression"] }

# Used for emitting log entries
log = "0.4.14"
# Used for rocket<->ruma conversions
http = "0.2.3"
# Used to find data directory for default db path
directories = "3.0.1"
# Used for ruma wrapper
serde_json = { version = "1.0.64", features = ["raw_value"] }
# Used for appservice registration files
serde_yaml = "0.8.17"
# Used for pdu definition
serde = "1.0.123"
# Used for secure identifiers
rand = "0.8.3"
# Used to hash passwords
rust-argon2 = "0.8.3"
# Used to send requests
reqwest = { version = "0.11.1" }
# Used for conduit::Error type
thiserror = "1.0.24"
# Used to generate thumbnails for images
image = { version = "0.23.14", default-features = false, features = ["jpeg", "png", "gif"] }
# Used to encode server public key
base64 = "0.13.0"
# Used when hashing the state
ring = "0.16.20"
# Used when querying the SRV record of other servers
trust-dns-resolver = "0.20.0"
# Used to find matching events for appservices
regex = "1.4.3"
# jwt jsonwebtokens
jsonwebtoken = "7.2.0"
# Performance measurements
tracing = "0.1.25"
opentelemetry = "0.12.0"
tracing-subscriber = "0.2.16"
tracing-opentelemetry = "0.11.0"
opentelemetry-jaeger = "0.11.0"
pretty_env_logger = "0.4.0"

[features]
default = ["conduit_bin"]
conduit_bin = [] # TODO: add rocket to this when it is optional
tls_vendored = ["reqwest/native-tls-vendored"]

[[bin]]
name = "conduit"
path = "src/main.rs"
required-features = ["conduit_bin"]

[lib]
name = "conduit"
path = "src/lib.rs"

[package.metadata.deb]
name = "matrix-conduit"
maintainer = "Paul van Tilburg <paul@luon.net>"
copyright = "2020, Timo Kösters <timo@koesters.xyz>"
license-file = ["LICENSE", "3"]
depends = "$auto, ca-certificates"
extended-description = """\
A fast Matrix homeserver that is optimized for smaller, personal servers, \
instead of a server that has high scalability."""
section = "net"
priority = "optional"
assets = [
  ["debian/env.local", "etc/matrix-conduit/local", "644"],
  ["debian/README.Debian", "usr/share/doc/matrix-conduit/", "644"],
  ["README.md", "usr/share/doc/matrix-conduit/", "644"],
  ["target/release/conduit", "usr/sbin/matrix-conduit", "755"],
]
conf-files = [
  "/etc/matrix-conduit/local"
]
maintainer-scripts = "debian/"
systemd-units = { unit-name = "matrix-conduit" }
